---
title: How to train your image classifier
---

[h&d](http://etherpad.hackersanddesigners.nl/p/networkedlabor)

We are going to be handling a "face detection" algorithm that's part of the OpenCV library. The algorithm has been described as performing:

> Rapid Object Detection With A Cascade of Boosted Classifiers Based on Haar-like Features

Let's take this sentence apart:

* [Rapid](https://en.wikipedia.org/wiki/Real-time_computing)
* [Object Detection](https://en.wikipedia.org/wiki/Object-class_detection)
* [Cascade](https://en.wikipedia.org/wiki/Cascading_classifiers)
* [Boosted](https://en.wikipedia.org/wiki/AdaBoost)
* [Classifiers](https://en.wikipedia.org/wiki/Statistical_classification)
* [Haar-like Features](https://en.wikipedia.org/wiki/Haar-like_features)


Links
---------

### Training Tutorials

* [opencv.org: Training User Guide](http://docs.opencv.org/2.4/doc/user_guide/ug_traincascade.html)
* [Jeff Thompson: MirrorTest NotesAndIdeas.txt July 2014](https://github.com/jeffThompson/MirrorTest/blob/master/NotesAndIdeas.txt) and [Training instructions](https://github.com/jeffThompson/MirrorTest/blob/master/TrainingInstructions.md)
* [Thorsten Ball's Train Your Own OpenCV Haar Classifier (The banana trainer, coding-robin.de, July 2013)](http://coding-robin.de/2013/07/22/train-your-own-opencv-haar-classifier.html)
* [Memememe's Training Haar Cascades Aug 2014](http://www.memememememememe.me/training-haar-cascades/)
* [Naotoshi Seo's Tutorial](http://note.sonots.com/SciSoftware/haartraining.html)
* [Training Haar-Cascade (gagan) Oct 2012](https://singhgaganpreet.wordpress.com/category/opencv/training-haar-cascade-opencv/)
* [Another python training script (memememe)](https://github.com/thiagohersan/memememe/tree/master/Python/createHaarCascade)

### Detection resources

* [opencv.org: Haar Feature-based Cascade Classifier for Object Detection](http://docs.opencv.org/2.4/modules/objdetect/doc/cascade_classification.html)
* [Face detection using python (tutorial)](http://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_objdetect/py_face_detection/py_face_detection.html)
* Viola Jones (the paper!)
* [https://en.wikipedia.org/wiki/Viola%E2%80%93Jones_object_detection_framework](https://en.wikipedia.org/wiki/Viola%E2%80%93Jones_object_detection_framework)
* Eigen Faces (the paper!)

### Raw material

* [Sample haar cascades](http://alereimondo.no-ip.org/OpenCV/34)
* https://github.com/inspirit/jsfeat/tree/master/cascades
* [UKBench](http://vis.uky.edu/~stewe/ukbench/) **LOCAL COPY AVAILABLE**
* [Face Recognition Databases](http://www.face-rec.org/databases/)
* [Computer Vision Test Images](http://www.cs.cmu.edu/~cil/v-images.html)

### Demo/Videos/interviews

* [In browser detection (Javascript implementation)](http://inspirit.github.io/jsfeat/sample_haar_face.html) [source](https://github.com/inspirit/jsfeat.git)
* [Face Machine (java applet demo of eigenfaces)](http://cognitrn.psych.indiana.edu/nsfgrant/FaceMachine/faceMachine.html)
* [Adam Harvey on Viola Jones](http://www.makematics.com/research/viola-jones/) ([direct vimeo link](https://vimeo.com/39561082))
* [Video Lecture on Face Detection (Ramsri Golla)](https://www.youtube.com/watch?v=WfdYYNamHZ8)
* [Viola-Jones Rapid Object Detection Project (Zack Smaridge, University of Florida)](https://www.youtube.com/watch?v=Wwn81tVIR10)
* [Jeff Thompson's Notes](https://github.com/jeffThompson/MirrorTest/blob/master/TrainingInstructions.md) for the MirrorTest project (very useful)
* [Adam Harvey CV Dazzle, etc videos](https://vimeo.com/adamhrv/videos)
* [HAAR face model visualisation based on OpenCV 2.4](https://www.youtube.com/watch?v=zLBAJ93-AEQ)
* [HAAR Cascade visualization, Tim van Oosterhout (Fabio!)](https://www.youtube.com/watch?v=L0JkjIwz2II) [code](https://github.com/Tubeliar/HAARCascadeVisualization)
* [Custom HAAR building interface](https://www.youtube.com/watch?v=90TLPmPbA_o) (no code, nor results shown ?!)
* [Obscenity Detection Using Haar-Like Features and Gentle Adaboost Classifier](http://www.hindawi.com/journals/tswj/2014/753860/), sadly no pictures
* [(Re)creating a HAAR cascade visualiser](https://timvanoosterhout.wordpress.com/2015/10/08/recreating-a-haar-cascade-visualiser/)

### Related
* [CMU PIE database](http://www.cs.cmu.edu/afs/cs/project/PIE/MultiPie/Multi-Pie/Home.html) [another link](http://www.flintbox.com/public/project/4742/)... did this used to be free?
* [SUN database](http://groups.csail.mit.edu/vision/SUN/)
* [Ada Boost](https://en.wikipedia.org/wiki/AdaBoost)
* [Eavise, KU Leven](http://www.eavise.be/)

### Art projects involving Face/Object Recognition / other CV

* [Mirror test, 2014](http://www.jeffreythompson.org/mirror-test.php) by [Jeff Thompson](http://www.jeffreythompson.org/) 
* [CV Dazzle](https://www.ahprojects.com/projects/cv-dazzle/) by [Adam Harvey](https://www.ahprojects.com/) 2010-ongoing
* [Facial Weaponization Communiqué: Fag Face: Zach Blas](https://vimeo.com/57882032) [Zach Blas](http://www.zachblas.info/)
* See also: [gay or eurotrash](http://blairmag.com/blair3/gaydar/euro.html)


### SICV

* [Vitrine](http://sicv.activearchives.org/vitrine/)
* [11 orderings / faces](http://guttormsgaard.activearchives.org/faces.html)

### People / Research

* [Rainer Lienhart](http://www.multimedia-computing.de/wiki/Prof._Dr._Rainer_Lienhart)


Local resources (to bring!)
------------------------------

* Mirror Test code + ebook (PDF)
* Videos (Adam Harvey with "boosted" audio ;)
* ukbench (useful as neg/bg material?)
* Camera
* Testing code (cascade visualiser)
* Vitrine Code + videos
* Videos/clips: They Live, Manchurian Candidate
* 2faces script / Kurenniemi!
* Example faces vec file! (to -show)
* scratch hard drive (extra space!)

Image Gallery
-----------------

![A "generated" sample with a "positive" image placed at random on top of a "negative" ](http://note.sonots.com/?plugin=ref&page=SciSoftware%2Fhaartraining&src=0001_0351_0227_0115_0115.jpg)

![Lena + Haar, Greg Borenstein](http://www.memememememememe.me/wp-content/uploads/2014/08/haarFace.jpg)

---

* Positive examples are all scaled to the same size (the example given is 20x20)
* Negative examples are "arbitrary" images at the same size
* Once trained, the classifier is applied to ROI (region of interest) of a "test image" and produces a binary result (match / no match)
* To search the entire image, the classifier is slid over the image at various scales (the classifier is scaled not the input image).


Techniques
---------------

* Decision trees

* Boosted classifiers
    * Discrete Adaboost
    * Real Adaboost
    * Gentle Adaboost
    * Logitboost

* Haar-like features
    * Edge
    * Line
    * Center-surround


Things to (possibly) do
-----------------------------

* IMPORTANT: webtrainer: implement LOAD annotations
* Make a sketch planning for things to do and when
* Produce a publication result from workshop (including participation from various particpants)
* Visualize (interactively) haar features (show how a given feature "sees" an image) / filters an image
* Visualize cascades themselves!
* Generate Cascades (from scratch? or as variations on existing ones)
* Face detect "glitch" code a la vitrine, but directly on camera / video input?
* Can you generate more permissive cascades from finished ones? 
* Other ways of generating .vec files ?!
* Creating a custom man page ???!!!


Code
---------
All the code for haartraining is in the opencv distribution at:

    apps/haartraining


Useful
--------

Adam Harvey rewrote an ActionScript implementation of Haar Cascade detection in Java to build the visualisations.

http://answers.opencv.org/question/58168/traincascade-train-dataset-for-temp-stage-can-not/

    You problem is pretty simple, basically your training says that it can no longer improve your cascade with the current training samples and settings beyond stage 3. (0 , 1 and 2). This is not an error, rather the algorithm not being able to continue. If you now run your algorithm again with -numStages 3 your model for detection will be generated.

    -minHitRate 0.999 -maxFalseAlarmRate 0.5 combined with only 5 positive samples will never work decently. Why putting your minHitRate so high? Start with increasing the amount of positive samples to 50 or 100. Also the ratio positives/negatives is very strange in this case. I would suspect it to be more balanced, like 500 pos and 1200 neg.

https://abhishek4273.com/2014/03/16/traincascade-and-car-detection-using-opencv/


1) The number of negative images must be greater than the number of positive images.

2) Try to set npos = 0.9 * number_of_positive_samples and 0.99 as a minHitRate.

3) vec-file has to contain >= (npos + (numStages-1) * (1 – minHitRate) * numPose) + S, where S is a count of samples from vec-file. S is a count of samples from vec-file that can be recognized as background right away.

