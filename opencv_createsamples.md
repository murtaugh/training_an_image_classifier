opencv_createsamples
==========================
[doc](http://docs.opencv.org/2.4/doc/user_guide/ug_traincascade.html#positive-samples)

The create samples application works in one of two modes:
* Synthesizing (randomly rotating a single fixed image)
* Extracting (taking portions of marked up images)

The -info parameter is particularly confusing. When used in combination with the -img option, it is an output parameter as in "synthesis mode"

The ultimate goal of the createsamples command is the creation of the *vec* file that represents a number of *positive* examples of your 

I: Synthesizing Using a single image as input
-----------------------------------

### input parameters

-w
-h
-img
-bg 
-num
[-bgcolor ]
[-bgthresh ]
[-inv]
[-randinv]
-maxxangle
-maxyangle
-maxzangle

### Output parameters

* -vec
* -info : output frames (jpeg by default) + info file (* output when combined with -img, without -img this is an INPUT variable!)
* -pngoutput : output png frames


II: Extracting image annotations 
---------------------------------------
[doc](http://docs.opencv.org/2.4/doc/user_guide/ug_traincascade.html#converting-the-marked-up-collection-of-samples-into-a-vec-format)

* -info
* -w
* -h
* -show
* -num

NB: The only effect num can have is to limit the samples to a certain number. It does not multiply the samples.
Example:

```
opencv_createsamples \
    -info annotations.txt \
    -w 24 \
    -h 24 \
    -vec positive.vec
```



COde
---------
```c
    /* determine action */
    if( imagename && vecname )
    {
        printf( "Create training samples from single image applying distortions...\n" );

        cvCreateTrainingSamples( vecname, imagename, bgcolor, bgthreshold, bgfilename,
                                 num, invert, maxintensitydev,
                                 maxxangle, maxyangle, maxzangle,
                                 showsamples, width, height );

        printf( "Done\n" );
    }
    else if( imagename && bgfilename && infoname)
    {
        printf( "Create data set from single image applying distortions...\n"
                "Output format: %s\n",
                (( pngoutput ) ? "PNG" : "JPG") );

        std::auto_ptr<DatasetGenerator> creator;
        if( pngoutput )
        {
            creator = std::auto_ptr<DatasetGenerator>( new PngDatasetGenerator( infoname ) );
        }
        else
        {
            creator = std::auto_ptr<DatasetGenerator>( new JpgDatasetGenerator( infoname ) );
        }
        creator->create( imagename, bgcolor, bgthreshold, bgfilename, num,
                        invert, maxintensitydev, maxxangle, maxyangle, maxzangle,
                        showsamples, width, height );

        printf( "Done\n" );
    }
    else if( infoname && vecname )
    {
        int total;

        printf( "Create training samples from images collection...\n" );

        total = cvCreateTrainingSamplesFromInfo( infoname, vecname, num, showsamples,
                                                 width, height );

        printf( "Done. Created %d samples\n", total );
    }
    else if( vecname )
    {
        printf( "View samples from vec file (press ESC to exit)...\n" );

        cvShowVecSamples( vecname, width, height, scale );

        printf( "Done\n" );
    }
    else
    {
        printf( "Nothing to do\n" );
    }
```
