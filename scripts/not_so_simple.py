import cv2
import numpy as np

cascade = cv2.CascadeClassifier("../haarcascades/haarcascade_frontalface_default.xml")
#frame = cv2.imread('queen.jpg')
#gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

# there are many options that can affect the detection
# features = cascade.detectMultiScale(gray, scaleFactor=1.25, minNeighbors=3, minSize=(50,50))

color = (255,0,0)

cap = cv2.VideoCapture(0)
print ("connecting to camera, press q to quit")
while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    features = cascade.detectMultiScale(gray)
    for (x,y,w,h) in features:
        print ((x, y, w, h))
        cv2.rectangle(frame, (x,y), (x+w,y+h), color, 2)

    # Display the resulting frame
    cv2.imshow('frame',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

#cv2.imwrite("output.png", frame)

    # roi_gray = gray[y:y+h, x:x+w]

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
