#!/usr/bin/env python
from __future__ import print_function
from argparse import ArgumentParser

p = ArgumentParser("")
p.add_argument("--boxsize", type=int, default=24)
p.add_argument("width", type=int)
p.add_argument("height", type=int)
args = p.parse_args()


w = args.width
h = args.height
s = args.boxsize

sw = s
sh = int(round(sw * (float(h)/w)))
if sh > s:
    sh = s
    sw = int(round(sh * (float(w)/h)))

print ("-w {0} -h {1}".format(sw, sh))

