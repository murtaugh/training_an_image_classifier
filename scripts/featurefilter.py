#!/usr/bin/env python

from __future__ import print_function
import cv2, os, datetime, sys
from argparse import ArgumentParser
from time import sleep
from Queue import Queue
from thread import start_new_thread
import numpy as np


p = ArgumentParser("Highlight features live, nb: requires haarcascade xml file(s)")
p.add_argument("--camera", type=int, default=0, help="camera number, default: 0")
p.add_argument("--output", default=None, help="path to save movie, default: None (show live)")
p.add_argument("--cascade", default="./haarcascades/haarcascade_frontalface_default.xml", help="location of the cascade XML file to use, default: ./haarcascades/haarcascade_frontalface_default.xml")
# p.add_argument("--cascade2", default="./haarcascades/haarcascade_eye.xml", help="optional secondary cascade xml")
p.add_argument("--cascade2", default="./haarcascades/haarcascade_eye.xml", help="optional secondary cascade xml")
p.add_argument("--scaleFactor", type=float, default=None, help="scaleFactor, float, default: None (1.1)")
p.add_argument("--minNeighbors", type=int, default=None, help="minNeighbors, int, default: None (3)")
p.add_argument("--minSize", type=int, default=None, help="minSize, int, default: None")
p.add_argument("--drawColor", default="255,0,0", help="color in b,g,r format, e.g. default: 255,0,0 (blue)")
p.add_argument("--drawWidth", type=int, default=2, help="draw line width, default: 2")
p.add_argument("--drawColor2", default="0,255,0")
p.add_argument("--drawWidth2", type=int, default=2)
p.add_argument("--delayFeature", type=int, default=1000)
p.add_argument("--delayDefault", type=int, default=10)
p.add_argument("--width", type=int, default=640, help="pre-detect resize width")
p.add_argument("--height", type=int, default=480, help="pre-detect resize height")
p.add_argument("--background", default="255,255,255")
p.add_argument("--nopad", default=False, action="store_true")
p.add_argument("--fourcc", default="XVID", help="fourcc code for output movie format, values: MJPG,mp4v,XVID(default)")
p.add_argument("--framerate", type=float, default=25, help="output frame rate")
args = p.parse_args()


cascade = cv2.CascadeClassifier(os.path.expanduser(args.cascade))

cascade2 = None
if args.cascade2:
    cascade2 = cv2.CascadeClassifier(os.path.expanduser(args.cascade2))

# cv2.namedWindow("display", cv2.cv.CV_WINDOW_NORMAL)
# cv2.setWindowProperty("display", cv2.WND_PROP_FULLSCREEN, cv2.cv.CV_WINDOW_FULLSCREEN)
# cap = cv2.VideoCapture()

color = tuple([int(x) for x in args.drawColor.split(",")])
color2 = tuple([int(x) for x in args.drawColor2.split(",")])
background = tuple([int(x) for x in args.background.split(",")])

def detectMultiScale (cascade, img, scaleFactor=None, minNeighbors=None, minSize=None):
    params = {}
    if scaleFactor:
        params['scaleFactor'] = scaleFactor
    if minNeighbors:
        params['minNeighbors'] = minNeighbors
    if minSize:
        params['minSize'] = (minSize, minSize)
    return cascade.detectMultiScale(img, **params)

def resize (img, w, h, interpolation = cv2.INTER_CUBIC):
    ih, iw, ic = img.shape
    if (ih > h) or (iw > w):
        # try fitting width
        sw = w
        sh = int(sw * (float(ih)/iw))
        if sh > h:
            # fit height instead
            sh = h
            sw = int(sh * (float(iw)/ih))
        return cv2.resize(img, (sw, sh), interpolation=interpolation)
    return img

def pad (img, w, h, color=(0, 0, 0)):
    ih, iw, ic = img.shape
    top = (h - ih) / 2
    bottom = h - ih - top
    left = (w - iw) / 2
    right = (w - iw - left)
    return cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)

def display_thread (frame_queue):
    global keep_running, count
    # print ("display_thread running...")
    out = None
    while True:
        # print ("dt...")
        if not frame_queue.empty():
            while not frame_queue.empty():
                frame = frame_queue.get()
            # process and display frame
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            features = detectMultiScale(cascade, gray, scaleFactor=args.scaleFactor, minNeighbors=args.minNeighbors, minSize=args.minSize)

            # new blank frame
            outframe = np.zeros(frame.shape, np.uint8)

            for (x,y,w,h) in features:
                # print ((x, y, w, h))
                count += 1
                roi_gray = gray[y:y+h, x:x+w]
                # roi_color = frame[y:y+h, x:x+w]
                # secondary cascade...
                if cascade2:
                    features2 = cascade2.detectMultiScale(roi_gray)
                    for (ex,ey,ew,eh) in features2:
                        fx = x+ex
                        fy = y+ey
                        # cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),color2,args.drawWidth2)
                        outframe[fy:fy+eh, fx:fx+ew] = frame[fy:fy+eh, fx:fx+ew]
                else:
                    outframe[y:y+h, x:x+w] = frame[y:y+h, x:x+w]

            if args.output:
                # VIDEO OUTPUT
                if out == None:
                    try:
                        fourcc = cv2.cv.CV_FOURCC(*args.fourcc)
                    except AttributeError:
                        fourcc = cv2.VideoWriter_fourcc(*args.fourcc)
                    out = cv2.VideoWriter()
                    out.open(args.output, fourcc, args.framerate, (args.width, args.height))
                framecount = 1
                for f in range(framecount):
                    out.write(outframe)
                # print ("{0}".format(i), file=sys.stderr)

            cv2.imshow('display', outframe)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                print ("QUIT")
                keep_running = False
                if out:
                    out.release()
                else:
                    cv2.destroyAllWindows()
                return
        else:
            sleep(0.25)

print ("Press q to stop...")
frame_queue = Queue()
start_new_thread(display_thread, (frame_queue, ))
count = 0
cap = cv2.VideoCapture(args.camera)
keep_running = True

while keep_running:
    ret, frame = cap.read()
    if frame == None:
        print ("ERROR CAPTURING FRAME. CHECK CAMERA CONNECTION AND SETTINGS", file=sys.stderr)
        sys.exit(0)
    frame_queue.put(frame)

cap.release()

print ("Found {0} features".format(count), file=sys.stderr)











