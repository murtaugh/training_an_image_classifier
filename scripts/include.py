#!/usr/bin/env python

"""
process:
...
#include "myfile.foo"
...
to inline the contents of a file

"""

from argparse import ArgumentParser
import sys, re


includepat = re.compile(r"^#include\s+\"?(.+?)\"?\s*$", re.I | re.M)

def process_line (l):
    m = includepat.search(line)
    if m:
        path = m.group(1)
        if args.list:
            sys.stdout.write(path)
        else:           
            try:
                with open(path) as fi:
                    for iline in fi:
                        sys.stdout.write(iline)
            except IOError as e:
                sys.stdout.write("include.py error ({0})".format(e))
        sys.stdout.write("\n")
    else:
        if not args.list:
            sys.stdout.write(line)

p = ArgumentParser("process simple linebased include directives")
p.add_argument("--list", action="store_true", default=False, help="output list of included files (makefile friendly!)")
p.add_argument("input", nargs="*", default=[])
args = p.parse_args()

if args.input:
    for n in args.input:
        with open(n) as f:
            for line in f:
                process_line(line)
else:
    for line in sys.stdin:
        process_line(line)



