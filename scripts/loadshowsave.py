import numpy as np
import cv2

img = cv2.imread('queen.jpg',0)
print ("showing image, press s to save")
cv2.imshow('image',img)
k = cv2.waitKey(0) & 0xFF
if k == ord('s'):
    print ("saving to output.png")
    cv2.imwrite('output.png',img)
cv2.destroyAllWindows()
