import cv2
import numpy as np

cascade = cv2.CascadeClassifier("../haarcascades/haarcascade_frontalface_default.xml")
frame = cv2.imread('queen.jpg')
gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

features = cascade.detectMultiScale(gray)
# there are many options that can affect the detection
# features = cascade.detectMultiScale(gray, scaleFactor=1.25, minNeighbors=3, minSize=(50,50))

color = (255,0,0)

for (x,y,w,h) in features:
    print ((x, y, w, h))
    cv2.rectangle(frame, (x,y), (x+w,y+h), color, 2)

cv2.imwrite("output.png", frame)

    # roi_gray = gray[y:y+h, x:x+w]
