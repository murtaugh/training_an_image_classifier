# find all .md files in the directory
mdsrc=$(shell ls *.md)
# map *.mp => *.html for mdsrc
html_from_md=$(mdsrc:%.md=%.html)

mp4=(ls shell *.mp4)
norm=(mp4:$.mp4=%.norm.mp4)

all: $(html_from_md)

%.norm.mp4: %.mp4
	ffmpeg -i $< -af 'pan=stereo:c0=FL:c1=FR' -ac 1 -af loudnorm=I=-16:TP=-1.5:LRA=11 -crf 23 $@


%.html: %.md
	python include.py < $< | \
	pandoc --from markdown \
		--to html \
		--standalone \
		--css styles.css \
		--template template.html \
		-o $@
		# --template template.html \

# special rule for debugging variables
print-%:
	@echo '$*=$($*)'
