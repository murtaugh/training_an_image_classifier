---
title: Trainings
---


M01: Michael's first successful training (following Nicolas)
---------------------------------------
Used scaled down sources (queen @ 50x50 and bg images at 100x100)

NB: -bgthresh 0 seems to be wrong

```bash
opencv_createsamples \
    -img queen_50x50.jpg \
    -bg images-sun-500-325.100x100.txt \
    -bgcolor 255 \
    -bgthresh 1 \
    -info info/info.lst \
    -pngoutput info \
    -maxxangle 0.5 \
    -maxyangle 0.5 \
    -maxzangle 0.5 \
    -num 1950

opencv_createsamples \
    -info info/info.lst \
    -num 1950 \
    -w 18 \
    -h 25 \
    -vec positives.vec

opencv_traincascade \
    -data data \
    -vec positives.vec \
    -bg images-sun-500-325.100x100.txt \
    -numPos 1800 \
    -numNeg 900 \
    -numStages 10 \
    -w 18 \
    -h 25 \
    -mem 2000
```


M02
---------------------------------------

Attempt with less resizing.

```bash
opencv_createsamples \
    -img queen.png \
    -bg images-sun-500-325.txt \
    -bgcolor 255 \
    -bgthresh 1 \
    -info info/info.lst \
    -pngoutput info \
    -maxxangle 0.5 \
    -maxyangle 0.5 \
    -maxzangle 0.5 \
    -num 1950

opencv_createsamples \
    -info info/info.lst -num 1950 \
    -w 18 -h 25 \
    -vec positives.vec
```

This produces a positives.vec as in M01, also 1.7MB in size.

```
mkdir data
opencv_traincascade \
    -data data \
    -vec positives.vec \
    -bg images-sun-500-325.txt \
    -numPos 1800 \
    -numNeg 900 \
    -numStages 10 \
    -w 18 \
    -h 25 \
    -mem 2000
```

Started at 14:19.

FULL output
```bash
PARAMETERS:
cascadeDirName: data
vecFileName: positives.vec
bgFileName: images-sun-500-325.txt
numPos: 1800
numNeg: 900
numStages: 10
precalcValBufSize[Mb] : 256
precalcIdxBufSize[Mb] : 256
stageType: BOOST
featureType: HAAR
sampleWidth: 18
sampleHeight: 25
boostType: GAB
minHitRate: 0.995
maxFalseAlarmRate: 0.5
weightTrimRate: 0.95
maxDepth: 1
maxWeakCount: 100
mode: BASIC

===== TRAINING 0-stage =====
<BEGIN
POS count : consumed   1800 : 1800
NEG count : acceptanceRatio    900 : 1
Precalculation time: 9
+----+---------+---------+
|  N |    HR   |    FA   |
+----+---------+---------+
|   1|        1|        1|
+----+---------+---------+
|   2| 0.999444|0.0922222|
+----+---------+---------+
END>
Parameters can not be written, because file data/params.xml can not be opened.
murtaugh@sonic:~/projects/SICV/training_an_image_classifier/training/queen3$ mkdir data
murtaugh@sonic:~/projects/SICV/training_an_image_classifier/training/queen3$ opencv_traincascade     -data data     -vec positives.vec     -bg images-sun-500-325.txt     -numPos 1800     -numNeg 900     -numStages 10     -w 18     -h 25     -mem 2000
PARAMETERS:
cascadeDirName: data
vecFileName: positives.vec
bgFileName: images-sun-500-325.txt
numPos: 1800
numNeg: 900
numStages: 10
precalcValBufSize[Mb] : 256
precalcIdxBufSize[Mb] : 256
stageType: BOOST
featureType: HAAR
sampleWidth: 18
sampleHeight: 25
boostType: GAB
minHitRate: 0.995
maxFalseAlarmRate: 0.5
weightTrimRate: 0.95
maxDepth: 1
maxWeakCount: 100
mode: BASIC

===== TRAINING 0-stage =====
<BEGIN
POS count : consumed   1800 : 1800
NEG count : acceptanceRatio    900 : 1
Precalculation time: 9
+----+---------+---------+
|  N |    HR   |    FA   |
+----+---------+---------+
|   1|        1|        1|
+----+---------+---------+
|   2| 0.999444|0.0922222|
+----+---------+---------+
END>
Training until now has taken 0 days 0 hours 2 minutes 3 seconds.

===== TRAINING 1-stage =====
<BEGIN
POS count : consumed   1800 : 1801
NEG count : acceptanceRatio    900 : 0.164805
Precalculation time: 10
+----+---------+---------+
|  N |    HR   |    FA   |
+----+---------+---------+
|   1|        1|        1|
+----+---------+---------+
|   2| 0.998889| 0.283333|
+----+---------+---------+
END>
Training until now has taken 0 days 0 hours 4 minutes 13 seconds.

===== TRAINING 2-stage =====
<BEGIN
POS count : consumed   1800 : 1803
NEG count : acceptanceRatio    900 : 0.0534569
Precalculation time: 9
+----+---------+---------+
|  N |    HR   |    FA   |
+----+---------+---------+
|   1|        1|        1|
+----+---------+---------+
|   2|        1|        1|
+----+---------+---------+
|   3| 0.996667| 0.417778|
+----+---------+---------+
END>
Training until now has taken 0 days 0 hours 7 minutes 7 seconds.

===== TRAINING 3-stage =====
<BEGIN
POS count : consumed   1800 : 1809
NEG count : acceptanceRatio    900 : 0.018005
Precalculation time: 9
+----+---------+---------+
|  N |    HR   |    FA   |
+----+---------+---------+
|   1|        1|        1|
+----+---------+---------+
|   2|        1|        1|
+----+---------+---------+
|   3|        1| 0.526667|
+----+---------+---------+
|   4| 0.996667|     0.31|
+----+---------+---------+
END>
Training until now has taken 0 days 0 hours 10 minutes 44 seconds.

===== TRAINING 4-stage =====
<BEGIN
POS count : consumed   1800 : 1815
NEG count : acceptanceRatio    900 : 0.00757053
Precalculation time: 9
+----+---------+---------+
|  N |    HR   |    FA   |
+----+---------+---------+
|   1|        1|        1|
+----+---------+---------+
|   2| 0.996111| 0.595556|
+----+---------+---------+
|   3| 0.996111| 0.595556|
+----+---------+---------+
|   4| 0.997778| 0.604444|
+----+---------+---------+
|   5| 0.996111| 0.436667|
+----+---------+---------+
END>
Training until now has taken 0 days 0 hours 15 minutes 10 seconds.

===== TRAINING 5-stage =====
<BEGIN
POS count : consumed   1800 : 1822
NEG count : acceptanceRatio    900 : 0.00281495
Precalculation time: 9
+----+---------+---------+
|  N |    HR   |    FA   |
+----+---------+---------+
|   1|        1|        1|
+----+---------+---------+
|   2|        1|        1|
+----+---------+---------+
|   3|        1| 0.771111|
+----+---------+---------+
|   4| 0.996667| 0.673333|
+----+---------+---------+
|   5| 0.996111| 0.517778|
+----+---------+---------+
|   6| 0.997222| 0.307778|
+----+---------+---------+
END>
Training until now has taken 0 days 0 hours 20 minutes 33 seconds.

===== TRAINING 6-stage =====
<BEGIN
POS count : consumed   1800 : 1828
NEG count : acceptanceRatio    900 : 0.00108522
Precalculation time: 10
+----+---------+---------+
|  N |    HR   |    FA   |
+----+---------+---------+
|   1|        1|        1|
+----+---------+---------+
|   2| 0.995556| 0.702222|
+----+---------+---------+
|   3| 0.997222| 0.847778|
+----+---------+---------+
|   4| 0.995556| 0.507778|
+----+---------+---------+
|   5| 0.996111| 0.583333|
+----+---------+---------+
|   6| 0.996111| 0.545556|
+----+---------+---------+
|   7| 0.996111| 0.412222|
+----+---------+---------+
END>
Training until now has taken 0 days 0 hours 26 minutes 27 seconds.

===== TRAINING 7-stage =====
<BEGIN
POS count : consumed   1800 : 1835
NEG count : acceptanceRatio    900 : 0.000493698
Required leaf false alarm rate achieved. Branch training terminated.
```

M03
--------
Question: Can multiple inputs be used instead of just a single image input to create samples?


