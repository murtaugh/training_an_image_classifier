(function () {

function extend () {
    for (var i=1, l=arguments.length; i<l; i++) {
        for (var key in arguments[i]) {
            arguments[0][key] = arguments[i][key]; 
        }
    }
    return arguments[0]
}

function _frames (elt, opts) {
    opts = extend ({}, {
        }, opts);   

    var that = {elt: elt},
        frames = [],
        img = document.createElement("img"),
        cur_frame_index = -1;

    elt.appendChild(img);
    // if (elt.style.position == "") { elt.style.position = "relative" };

    function update_frame () {
        img.src = (cur_frame_index >= 0 && cur_frame_index < frames.length) ? frames[cur_frame_index].src : "";
        if (opts.frame) {
            opts.frame.call(that, cur_frame_index);
        }
    }

    that.add = function (url) {
        frames.push({src: url});
        cur_frame_index = frames.length - 1;
        if (opts.numframeschanged) {
            opts.numframeschanged.call(that, frames.length);
        }
        update_frame();
    }

    that.length = function () {
        return frames.length;
    }

    that.remove = function (i) {
        // i = (i === undefined) ? cur_frame_index : i;
        frames.splice(i, 1);
        console.log("remove", i, frames.length);;
        if (opts.numframeschanged) {
            opts.numframeschanged.call(that, frames.length);
        }
        cur_frame_index = Math.min(frames.length-1, Math.max(0, cur_frame_index));
        update_frame();        
    }

    that.removeAll = function () {
        if (frames.length > 0) {
            frames = [];
            cur_frame_index = -1;

            if (opts.numframeschanged) {
                opts.numframeschanged.call(that, frames.length);
            }            
            update_frame();
        }
    }

    that.index = function (i) {
        if (i === undefined) {
            // getter
            return cur_frame_index;
        } else {
            // setter
            i = parseInt(i);
            if (cur_frame_index !== i && i >=0 && i < frames.length) {
                cur_frame_index = i;
                update_frame();
            }
        }
    }

    that.get = function (i) {
        if (i === undefined) {
            return frames;
        } else {
            return frames[i];
        }
    }

    return that;
}

window._frames = _frames;

})(window);
