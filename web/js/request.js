function cc_xhr (url, responseType, callback, method, data, dataEncoding) {
    var httpRequest;
    if (window.XMLHttpRequest) { // Mozilla, Safari, IE7+ ...
        httpRequest = new XMLHttpRequest();
    } else if (window.ActiveXObject) { // IE 6 and older
        try { httpRequest = new ActiveXObject("Msxml2.XMLHTTP"); } 
        catch (e) {
            try { httpRequest = new ActiveXObject("Microsoft.XMLHTTP"); } 
            catch (e) {}
        }
    }
    httpRequest.onreadystatechange = function(){
        // process the server response
        // console.log("readystatechange", this.readyState);   
        var response;
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
                if (responseType == "xml") {
                    response = httpRequest.responseXML; }
                else if (responseType == "json") {
                    response = JSON.parse(httpRequest.responseText); }
                else {
                    response = httpRequest.responseText }
                callback.call(httpRequest, response);
            } else {
                // there was a problem with the request,
                // for example the response may contain a 404 (Not Found)
                // or 500 (Internal Server Error) response code
            }
        } else {
        // still not ready
        }
    };
    httpRequest.onerror = function (err) {
        console.log("error", err);
    };
    httpRequest.open(method || 'GET', url, true);
    if (data === undefined) {
        httpRequest.send(null);
    } else {
        if (dataEncoding == 'json') {
            var out_data = JSON.stringify(data);
            httpRequest.setRequestHeader('Content-Type', 'application/json');
            // chrome doesn't like setting content-length
            // httpRequest.setRequestHeader('Content-Length', out_data.length);
            httpRequest.send(out_data);
        } else {
            httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            // httpRequest.setRequestHeader('Content-Length', data.length);
            httpRequest.send(data);
        }
    }
    return httpRequest;
}
