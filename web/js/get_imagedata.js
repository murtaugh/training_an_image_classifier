function get_imagedata (imgelt, width, height, callback) {
    function _callback () {
        var canvas = document.createElement("canvas"),
            ctx = canvas.getContext("2d");

        canvas.width = width;
        canvas.height = height; 
        ctx.drawImage(imgelt, 0, 0, width, height);

        var data = ctx.getImageData(0, 0, width, height);
        imgelt.removeEventListener(_callback);
        if (opts.callback) {
            callback.call(imgelt, data);
        }
    }
    if (imgelt.complete && imgelt.naturalHeight) {
        _callback.call(imgelt);
    } else {
        imgelt.addEventListener(_callback, false);
    }
}
