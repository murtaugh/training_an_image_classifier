function extend () {
    for (var i=1, l=arguments.length; i<l; i++) {
        for (var key in arguments[i]) {
            arguments[0][key] = arguments[i][key]; 
        }
    }
    return arguments[0]
}
function dropimage (elt, opts) {
    var that = {elt: elt},
        canvas, ctx;
    opts = extend({}, {
            box_size: 256,
            scale: true, // add borders to preserve ratio
            pad: true, // center image in box
            gray: true
        }, opts);

    if (elt.style.position == "") { elt.style.position = "relative"; }
    function init_canvas() {
        canvas = document.createElement("canvas"),
        ctx = canvas.getContext("2d");
        canvas.style.width = opts.box_size + "px";
        canvas.style.height = opts.box_size + "px";
        canvas.style.position = "absolute";
        canvas.style.left = "0";
        canvas.style.top = "0";        
        elt.appendChild(canvas);
    }

    function imgdata_grayscale (data) {
        for (var i=0, len = data.length; i<len; i+=4) {
            var r = data[i],
                g = data[i+1],
                b = data[i+2],
                gray = (r+g+b)/3
            data[i] = gray;
            data[i+1] = gray;
            data[i+2] = gray;
        }
    }

    function cancel(e) {
        if (e.preventDefault) { e.preventDefault(); }
        return false;
    }

    var img = new Image();
    img.addEventListener("load", function () {
        console.log("dropimage: img.load");
        var iw = img.width,
            ih = img.height,
            sw, sh;
        if (opts.scale) {
            sw = opts.box_size;
            sh = (ih/iw)*sw;
            if (sh > opts.box_size) {
                sh = opts.box_size;
                sw = (iw/ih)*sh;
            }                
        } else {
            sw = opts.box_size;
            sh = opts.box_size;
        }
        var px = opts.pad ? (opts.box_size-sw)/2 : 0,
            py = opts.pad ? (opts.box_size-sh)/2 : 0;
        // console.log("sizes", iw, ih, sw, sh, px, py);

        if (canvas == undefined) { init_canvas(); }
        canvas.width = opts.box_size;
        canvas.height = opts.box_size;
        ctx.drawImage(img, px, py, sw, sh);
        var imgdata = ctx.getImageData(0, 0, 256, 256);
        // console.log("check", data.data.length, data.data.length/4, opts.box_size*opts.box_size);
        if (opts.gray) {
            imgdata_grayscale(imgdata.data);
        }
        ctx.putImageData(imgdata, 0, 0);
        that.imgdata = imgdata;
        if (opts.pixels) {
            opts.pixels.call(that);
        }
        // console.log("data", imgdata.data, imgdata.data.length);

    });
    img.addEventListener("error", function (e) {
        console.log("error", e);
    })

    elt.addEventListener("drop", function (e) {
        e.preventDefault();
        var dt    = e.dataTransfer,
            files = dt.files;
        console.log("drop", e, dt, dt.files);
        if (dt.files.length > 0) {
            console.log("imagedrop: processing files");
            for (var i=0; i<files.length; i++) {
                var file = files[i],
                    reader = new FileReader();
                // console.log("file", i, file);
                //attach event handlers here...
                reader.addEventListener('loadend', function (e) {
                    // console.log('loadend');
                    img.src = reader.result;
                }, false);
                reader.readAsDataURL(file);
                // will trigger image load
            }
        } else {
            var url = dt.getData("text/uri-list");
            console.log("imagedrop: url", url);
            
            if (url) {
                console.log("setting img.src", url);
                img.src = url;
            }
            
        }
    }, false);
    elt.addEventListener("dragover", cancel);
    elt.addEventListener("dragenter", cancel);
    return that;
}
