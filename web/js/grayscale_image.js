function grayscale_image (imgelt, width, height, callback, pixelcallback) {
    var deferred = false;
    function _callback () {
        if (deferred) { imgelt.removeEventListener("load", _callback); }

        var canvas = document.createElement("canvas"),
            ctx = canvas.getContext("2d"),
            gray_image;

        canvas.width = width;
        canvas.height = height;
        var sr = boxfit(imgelt.naturalWidth, imgelt.naturalHeight, width, height); 
        ctx.drawImage(imgelt, sr.padx, sr.pady, sr.width, sr.height);

        var imagedata = ctx.getImageData(0, 0, width, height),
            gray_data= ctx.createImageData(imagedata);

        // console.log("processing imagedata", imagedata.width, imagedata.height);

        for (var y=0; y<imagedata.height; y++) {
            for (var x=0; x<imagedata.width; x++) {
                var i = ((y * imagedata.width) + x) * 4,
                    r = imagedata.data[i],
                    g = imagedata.data[i+1],
                    b = imagedata.data[i+2],
                    alpha = imagedata.data[i+3],
                    gray = Math.floor((r+g+b)/3);

                gray_data.data[i] = gray;
                gray_data.data[i+1] = gray;
                gray_data.data[i+2] = gray;
                gray_data.data[i+3] = alpha;
                // if (y == 128) { console.log("gray", x, y, gray) }
                if (pixelcallback) { pixelcallback.call(imgelt, x, y, gray); }
            }
        }
        ctx.putImageData(gray_data, 0, 0);
        var dataURL = canvas.toDataURL();
        callback.call(imgelt, dataURL);
    }
    if (imgelt.complete && imgelt.naturalHeight) {
        _callback.call(imgelt);
    } else {
        deferred = true;
        imgelt.addEventListener("load", _callback, false);
    }
}
