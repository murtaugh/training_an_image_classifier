function boxfit (w, h, boxw, boxh, scale_up) {
    // fit (w, h) into (boxw, boxh) preserving aspect ratio
    // and padding to center in box
    // unless scale_up is set to True, will never scale up
    // returns { width: scaled_width, height: , padx: , pady: }
    var sw = w,
        sh = h;

    if (scale_up || (w > boxw) || (h > boxh)) {
        sw = boxw;
        sh = (h/w) * sw;

        if (sh > boxh) {
            sh = boxh;
            sw = (w/h) * sh;
        }
    }

    return {
        width: sw,
        height: sh,
        padx: Math.floor((boxw-sw)/2),
        pady: Math.floor((boxh-sh)/2)
    }
}
