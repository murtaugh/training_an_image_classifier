function cc_event_mouse_pos (e, elt) {
    // return event mouse position in document coordinates
    var posx = 0,
        posy = 0;
    if (!e) { e = window.event; }
    if (e.pageX || e.pageY) {
        posx = e.pageX;
        posy = e.pageY;
    } else if (e.clientX || e.clientY) {
        posx = e.clientX + document.body.scrollLeft
            + document.documentElement.scrollLeft;
        posy = e.clientY + document.body.scrollTop
            + document.documentElement.scrollTop;
    }
    // posx and posy contain the mouse position relative to the document
    var ret = { left: posx, top: posy };
    if (elt) {
        var eltpos = cc_elt_pos(elt);
        ret.left -= eltpos.left;
        ret.top -= eltpos.top;
    }
    return ret;
}


// https://developer.mozilla.org/en-US/docs/Web/API/Element/getBoundingClientRect
function cc_elt_pos (elt) {
    var r = elt.getBoundingClientRect(elt),
        sx = window.pageXOffset || window.scrollX,
        sy = window.pageYOffset || window.scrollY;
    return {
        left: r.left + sx,
        top: r.top + sy,
        right: r.right + sx,
        bottom: r.bottom + sy 
    };
}
