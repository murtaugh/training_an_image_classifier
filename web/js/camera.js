(function () {

function extend () {
    for (var i=1, l=arguments.length; i<l; i++) {
        for (var key in arguments[i]) {
            arguments[0][key] = arguments[i][key]; 
        }
    }
    return arguments[0]
}

function _camera (elt, opts) {
    opts = extend ({}, {
        }, opts);   

    var that = {elt: elt},
        IDLE = 0,
        CONNECTING = 1,
        CONNECTED = 2,
        state = IDLE,
        video,
        canvas = null,
        ctx = null,
        camera_stream,
        info = document.createElement("div");

    elt.appendChild(info);
    if (elt.style.position == "") { elt.style.position = "relative" };

    that.connect = function () {
        connect();
    }
    that.disconnect = function () {
        disconnect();
    }
    that.connected = function () {
        return state == CONNECTED;
    }

    function connect (width, height) {
        if (state === IDLE) {
            state = CONNECTING;
            var constraints = { audio: false, video: { width: width, height: height } };
            navigator.mediaDevices
                .getUserMedia(constraints)
                .then(function(stream) {
                    camera_stream = stream;
                    video = document.createElement('video');
                    video.style.position = "absolute";
                    video.style.zIndex = 0;
                    video.style.left = "0px";
                    video.style.top = "0px";

                    elt.appendChild(video);

                    video.src = window.URL.createObjectURL(stream);
                    video.onloadedmetadata = function(e) {
                        video.play();
                    };
                    state = CONNECTED;
                    info.textContent = "Connected";
                    // console.log("opts.connect", opts.connect);
                    if (opts.connect) {
                        // console.log("call connect callback")
                        opts.connect.call(this);
                    }
                })
               .catch(function(err) {
                    console.log(err.name + ": " + err.message);
                    info.textContent = "Error opening connection to camera, ensure a camera is connected (" + err.message + ")";
                    state = IDLE;
                    if (opts.error) {
                        opts.error.call(this, err);
                    }
                });            
        }
    }
    function disconnect () {
        if (state === CONNECTED) {
            video.pause();
            // http://stackoverflow.com/questions/11642926/stop-close-webcam-which-is-opened-by-navigator-getusermedia
            camera_stream.getVideoTracks()[0].stop();
            elt.removeChild(video);
            video = undefined;
            camera_stream = undefined;
            connected = false;
            state = IDLE;
            info.textContent = "";
            if (opts.disconnect) {
                opts.disconnect.call(this);
            }
        }
    }

    that.get_frame = function () {
        if (state == CONNECTED) {
            if (canvas == null) {
                // console.log("creating canvas");
                canvas = document.createElement("canvas");
                // elt.appendChild(canvas);
                canvas.width = video.videoWidth;
                canvas.height = video.videoHeight;
                ctx = canvas.getContext("2d");
            }
            ctx.drawImage(video, 0, 0);
            var imageData = canvas.toDataURL();
            return imageData;
        }
    }
    return that;
}

window._camera = _camera;

})(window);
