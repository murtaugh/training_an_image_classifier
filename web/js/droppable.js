(function (window) {

function extend () {
    for (var i=1, l=arguments.length; i<l; i++) {
        for (var key in arguments[i]) {
            arguments[0][key] = arguments[i][key]; 
        }
    }
    return arguments[0]
}

function droppable (elt, opts) {
    var that = {elt: elt};

    opts = extend({}, opts);

    // if (elt.style.position == "") { elt.style.position = "relative"; }

    function cancel(e) {
        if (e.preventDefault) { e.preventDefault(); }
        return false;
    }
    elt.addEventListener("drop", function (e) {
        e.preventDefault();
        var dt    = e.dataTransfer,
            files = dt.files;
        console.log("drop", e, dt, dt.files);
        if (dt.files.length > 0) {
            for (var i=0; i<files.length; i++) {
                var file = files[i],
                    reader = new FileReader();

                // console.log("file", i, file);
                //attach event handlers here...
                reader.addEventListener('loadend', function (e) {
                    // console.log('loadend');
                    img.src = reader.result;
                }, false);
                reader.readAsDataURL(file);
                // will trigger image load
            }
        } else {
            var url = dt.getData("text/uri-list");
            console.log("imagedrop: url", url);
            
            if (url) {
                console.log("setting img.src", url);
                img.src = url;
            }
            
        }
    }, false);
    elt.addEventListener("dragover", cancel);
    elt.addEventListener("dragenter", function (e) {
        if (e.preventDefault) { e.preventDefault(); }
        if (opts.dragenter) { opts.dragenter.call(that); }
        return false;
    });
    return that;
}

window.droppable = droppable;

function droppableimage (elt, opts) {
    var that = droppable(elt, opts);
    opts = extend({}, opts);



    return that;
}

})(window);
