(function () {

function extend () {
    for (var i=1, l=arguments.length; i<l; i++) {
        for (var key in arguments[i]) {
            arguments[0][key] = arguments[i][key]; 
        }
    }
    return arguments[0]
}

function cc_event_mouse_pos (e, elt) {
    // return event mouse position in document coordinates
    var posx = 0,
        posy = 0;
    if (!e) { e = window.event; }
    if (e.pageX || e.pageY) {
        posx = e.pageX;
        posy = e.pageY;
    } else if (e.clientX || e.clientY) {
        posx = e.clientX + document.body.scrollLeft
            + document.documentElement.scrollLeft;
        posy = e.clientY + document.body.scrollTop
            + document.documentElement.scrollTop;
    }
    // posx and posy contain the mouse position relative to the document
    var ret = { left: posx, top: posy };
    if (elt) {
        var eltpos = cc_elt_pos(elt);
        ret.left -= eltpos.left;
        ret.top -= eltpos.top;
    }
    return ret;
}

// https://developer.mozilla.org/en-US/docs/Web/API/Element/getBoundingClientRect
function cc_elt_pos (elt) {
    var r = elt.getBoundingClientRect(elt),
        sx = window.pageXOffset || window.scrollX,
        sy = window.pageYOffset || window.scrollY;
    return {
        left: r.left + sx,
        top: r.top + sy,
        right: r.right + sx,
        bottom: r.bottom + sy 
    };
}

function _annotations (elt, opts) {
    opts = extend ({}, {
        boxclassname: "annotationbox",
        locked: false, /* when true: disallows selecting, deleting, marquee draw to add */
        lockAspectRatio: false,  /* always lock aspect ratio of boxes */
        changeOnResize: false, /* trigger change callbacks on resize */
        changeOnDrag: false, /* trigger change callbacks on drag */
        lockAspectRatioWidth: 1,
        lockAspectRatioHeight: 1
    }, opts);   

    var that = {elt: elt},
        annotation = that,
        boxes = [],
        marquee = false,
        marquee_start,
        marquee_box = null;

    that.lockAspectRatio = function (w, h) {
        if (w === undefined) {
            // getter
            return {w: opts.lockAspectRatioWidth, h: opts.lockAspectRatioHeight}
        }
        // setter
        if (h === undefined) {
            // 1 param: assume true / false value
            opts.lockAspectRatio = (w === true);
        } else {
            // 2 params: w, h -- set lockAspect to true
            opts.lockAspectRatio = true;
            opts.lockAspectRatioWidth = w;
            opts.lockAspectRatioHeight = h;
        }
    }

    function select (box) {
        boxes.forEach(function (box) {
            box.elt.classList.remove("selected");
        });
        if (opts.locked) { return };
        if (box) {
            box.elt.classList.add("selected");
        }
    }

    // elt.style.width = opts.width + "px";
    // elt.style.height = opts.height + "px";

    // MARQUEE DRAWING...
    elt.addEventListener("mousedown", function (e) {
        // console.log("annotations.click", e.target);
        if ((e.target == elt) && !opts.locked) {
            select();
            // start a possible marquee draw ?
            marquee = true;
            marquee_start = cc_event_mouse_pos(e, elt);
        }
    });
    elt.addEventListener("mousemove", function (e) {
        if (marquee) {
            var m = cc_event_mouse_pos(e, elt),
                m_left = m.left,
                m_top = m.top,
                dx = (m.left - marquee_start.left),
                dy = (m.top - marquee_start.top);
 
            if (opts.lockAspectRatio) {
                var ndy = Math.abs(dx) * (opts.lockAspectRatioHeight/opts.lockAspectRatioWidth),
                    ndx = Math.abs(dy) * (opts.lockAspectRatioWidth/opts.lockAspectRatioHeight);
                // keep larger change, preserving the original sign
                if (ndx > Math.abs(dx)) {
                    dx = dx < 0 ? -ndx : ndx;
                } else {
                    dy = dy < 0 ? -ndy : ndy;
                }
            }
    
            m_left = marquee_start.left + dx;
            m_top = marquee_start.top + dy;

            if ((dx > 2 || dy > 2 || dx < -2 || dy < -2) && marquee_box === null) {
                marquee_box = _crop_box(document.createElement("div"));
                elt.appendChild(marquee_box.elt);
            }
            if (marquee_box !== null) {
                var x = Math.min(marquee_start.left, m_left),
                    y = Math.min(marquee_start.top, m_top);
                marquee_box.elt.style.left = x+"px";
                marquee_box.elt.style.top = y+"px";
                marquee_box.elt.style.width = Math.abs(dx)+"px";
                marquee_box.elt.style.height = Math.abs(dy)+"px";                
            } 
        }
    });
    elt.addEventListener("mouseup", function (e) {
        if (marquee) {
            marquee = false;
            if (marquee_box !== null) {
                var w = parseInt(marquee_box.elt.style.width),
                    h = parseInt(marquee_box.elt.style.height);
                if (w > 10 || h > 10) {
                    boxes.push(marquee_box);
                    select(marquee_box);
                    signal_changes();
                } else {
                    // too small, just remove
                    elt.removeChild(marquee_box.elt);
                }
                marquee_box = null;
            }
        }
    });

    document.addEventListener("keydown", function (e) {
        // console.log("keypress", e);
        if ((e.keyCode == 8) && !opts.locked) { // backspace removes boxes
            for (var i=0; i<boxes.length; i++) {
                var box = boxes[i];
                if (box.elt.classList.contains("selected")) {
                    elt.removeChild(box.elt);
                    boxes.splice(i--, 1);
                    signal_changes();
                }
            }
        }
    })


    that.set = function (rects) {
        var rlen = rects.length,
            i, box;
        // exit
        while (boxes.length > rlen) {
            // remove box
            box = boxes.pop();
            elt.removeChild(box.elt);
        }
        // enter
        for (i=boxes.length; i<rlen; i++) {
            box = _crop_box(document.createElement("div"));
            boxes.push(box);
            elt.appendChild(box.elt);
        }
        // update
        for (i=0; i<rlen; i++) {
            boxes[i].elt.style.left = rects[i].x + "px";
            boxes[i].elt.style.top = rects[i].y + "px";
            boxes[i].elt.style.width = rects[i].width + "px";
            boxes[i].elt.style.height = rects[i].height + "px";
        }
    }

    function signal_changes () {
        if (opts.change) {
            var data = boxes.map(function (b) {
                return b.json();
            })
            opts.change.call(that, data);
        }
    }


    /* crop_box */
    function _crop_box (elt, cbopts) {
        cbopts = extend ({}, {
            width: 100,
            height: 100,
            border: opts.border
        }, cbopts);   

        var that = {elt: elt};

        elt.classList.add(opts.boxclassname);
        elt.style.position = "absolute";
        elt.style.left = 0;
        elt.style.top = 0;
        elt.style.zIndex = 2;  
        elt.style.width = cbopts.width+"px";
        elt.style.height = cbopts.height+"px";
        elt.style.boxSizing = "border-box";
        // elt.style.border = opts.border;
        // elt.appendChild(crop);
        // interact(elt).resizable({ preserveAspectRatio: true })
        
        var parent_width, parent_height;

        interact(elt)
          .draggable({
            onstart: function (event) {
                var b = annotation.elt.getBoundingClientRect();
                parent_width = b.right - b.left;
                parent_height = b.bottom - b.top;
                // console.log("dragstart", parent_width, parent_height);
            },
            onmove: function (event) {
                var target = event.target,
                    // keep the dragged position in the data-x/data-y attributes
                    x = (parseInt(target.style.left) || 0) + event.dx,
                    w = (parseInt(target.style.width) || 0),
                    y = (parseInt(target.style.top) || 0) + event.dy,
                    h = (parseInt(target.style.height) || 0);

                
                x = Math.min((parent_width - w), Math.max(0, x));
                y = Math.min((parent_height - h), Math.max(0, y));

                target.style.left = Math.round(x)+"px";
                target.style.top = Math.round(y)+"px";

                if (opts.changeOnDrag) {
                    signal_changes();
                }
            }
          })
          .resizable({
            preserveAspectRatio: false,
            edges: { left: true, right: true, bottom: true, top: true }
          })
          .on("mousedown", function (event) {
            select(that);
            // interact(elt).resizable({ preserveAspectRatio: event.ctrlKey || opts.lockAspectRatio })

          })
          .on('resizestart', function (event) {
                var b = annotation.elt.getBoundingClientRect();
                parent_width = b.right - b.left;
                parent_height = b.bottom - b.top;
                // console.log("resizestart", parent_width, parent_height);
          })
          .on('resizemove', function (event) {
            var target = event.target,
                x = (parseInt(target.style.left) || 0),
                y = (parseInt(target.style.top) || 0),
                w = Math.round(event.rect.width),
                h = Math.round(event.rect.height);

            // constrain values to parent
            w = Math.min((parent_width - x), Math.max(0, w));
            h = Math.min((parent_height - y), Math.max(0, h));
            x = Math.min((parent_width - w), Math.max(0, x));
            y = Math.min((parent_height - h), Math.max(0, y));

            // constrain w/h to aspect ratio if set to limit
            // console.log("event", event);
            if (opts.lockAspectRatio) {
                // var ch, cw;
                if (event.edges.right || event.edges.left) {
                    // set h from w
                    h = w * (opts.lockAspectRatioHeight/opts.lockAspectRatioWidth);
                } else {
                    // set w from h
                    w = h * (opts.lockAspectRatioWidth/opts.lockAspectRatioHeight);
                }
            }

            target.style.width  = w + 'px';
            target.style.height = h + 'px';
            x += event.deltaRect.left;
            y += event.deltaRect.top;
            target.style.left = Math.round(x)+"px";
            target.style.top = Math.round(y)+"px";

            if (opts.changeOnResize) { signal_changes(); }
            // target.textContent = w+'×'+h;
          })
          .on("resizeend", function (event) {
            signal_changes();
          })
          .on("dragstart", function (event) {
            select(that);
          })
          .on("dragend", function (event) {
            signal_changes();
          })
          .on("tap", function (event) {
            // console.log("tap", elt);
            select(that);
          });

        that.json = function () {
            return {
                x: parseInt(elt.style.left),
                y: parseInt(elt.style.top),
                width: parseInt(elt.style.width),
                height: parseInt(elt.style.height)
            }
        }

        return that;        
    }

    return that;
}

window._annotations = _annotations;

})(window);
