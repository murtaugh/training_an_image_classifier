function integral_image (imgelt, width, height, callback, pixelcallback) {
    var that = {},
        data = [],
        ready = false,
        deferred = false;
    that.data = data;
    that.ready = function () {
        return ready;
    }
    function _callback () {
        if (deferred) { imgelt.removeEventListener("load", _callback); }

        var canvas = document.createElement("canvas"),
            ctx = canvas.getContext("2d"),
            gray_image;

        canvas.width = width;
        canvas.height = height;
        var sr = boxfit(imgelt.naturalWidth, imgelt.naturalHeight, width, height); 
        ctx.drawImage(imgelt, sr.padx, sr.pady, sr.width, sr.height);

        var imagedata = ctx.getImageData(0, 0, width, height),
            gray_data= ctx.createImageData(imagedata);

        // console.log("processing imagedata", imagedata.width, imagedata.height);

        for (var y=0; y<imagedata.height; y++) {
            data[y] = [];
            curline = 0;
            for (var x=0; x<imagedata.width; x++) {
                var i = ((y * imagedata.width) + x) * 4,
                    r = imagedata.data[i],
                    g = imagedata.data[i+1],
                    b = imagedata.data[i+2],
                    alpha = imagedata.data[i+3],
                    gray = Math.floor((r+g+b)/3);

                // for display
                gray_data.data[i] = gray;
                gray_data.data[i+1] = gray;
                gray_data.data[i+2] = gray;
                gray_data.data[i+3] = alpha;

                var above = (y > 0) ? data[y-1][x] : 0;
                curline += gray;
                data[y][x] = above + curline;

                // if (y == 128) { console.log("gray", x, y, gray) }
                if (pixelcallback) { pixelcallback.call(imgelt, x, y, gray); }
            }
        }
        ctx.putImageData(gray_data, 0, 0);
        var dataURL = canvas.toDataURL();
        ready = true;
        callback.call(that, dataURL);
    }
    if (imgelt.complete && imgelt.naturalHeight) {
        window.setTimeout(function () {
            _callback.call(imgelt);
        }, 0);
    } else {
        deferred = true;
        imgelt.addEventListener("load", _callback, false);
    }
    /*
    note: the integral image at x,y represents the sum of intensities
    *above* and to the *left* of the given pixel
    (not including the pixel/row/col at x,y itself)
    data[x,y] includes the pixel itself thus represents the sum of x+1, y+1
    */

    that.getArea = function (x, y, width, height) {
        if (!ready) { return 0 }
        // console.log("getArea", x, y, width, height);
        // why the -1's? : see the note above
        var bottom = y + height,
            right = x + width,
            a = (x == 0 || y == 0) ? 0 : that.data[y-1][x-1],
            b = (x == 0) ? 0 : that.data[bottom-1][x-1],
            c = (y == 0) ? 0 : that.data[y-1][right-1],
            d = that.data[bottom-1][right-1];
        // console.log("a", a, "b", b, "c", c, "d", d);
        return (d + a - b - c);
    }
    return that;
}
