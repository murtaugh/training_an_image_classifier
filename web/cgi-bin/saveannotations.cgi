#!/usr/bin/env python

import cgitb; cgitb.enable()
import sys, json
import os, cgi, urllib


"""
output layout

project/obj.idx
project/obj/framexxx.png
project/bg.idx
project/bg/framexxx.png
"""


def stringifyrect (r):
    return "{0} {1} {2} {3}".format(r['x'], r['y'], r['width'], r['height'])

method = os.environ.get("REQUEST_METHOD", "GET")
result = {'success': False};
oc, bc = 0, 0

if method == "POST":
    fs = cgi.FieldStorage()
    data = json.loads(fs.getvalue("data"))
    path = fs.getvalue("path", ".")

    objpath = fs.getvalue("objpath", "obj-info.txt")
    bgpath = fs.getvalue("bgpath", "bg.txt")
    objpath = os.path.join(path, objpath)
    bgpath = os.path.join(path, bgpath)

    objidx = None # open(objpath, "w")
    bgidx = None # open(bgpath, "w")

    for frame in data['frames']:
        if len(frame['rects']) > 0:
            rpath = os.path.relpath(frame['src'], path)
            line = "{0} {1} {2}".format(rpath, len(frame['rects']), ' '.join([stringifyrect(r) for r in frame['rects']]))
            if objidx == None:
                objidx = open(objpath, "w")
            objidx.write(line + "\n")
            oc += 1
        else:
            rpath = os.path.relpath(frame['src'], path)
            line = "{0}".format(rpath)
            if bgidx == None:
                bgidx = open(bgpath, "w")
            bgidx.write(line + "\n")
            bc += 1 
    result['success'] = True
    result['objcount'] = oc
    result['bgcount'] = bc
    result['message'] = "Wrote {0} objects, {1} backgrounds".format(oc, bc)
    if objidx != None:
        objidx.close()
    if bgidx != None:
        bgidx.close()

print 'Content-Type: application/json'
print
print json.dumps(result)
