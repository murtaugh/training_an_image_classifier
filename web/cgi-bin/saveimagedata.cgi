#!/usr/bin/env python

"""

Save a data url image to file (assumed to be image/png)

requires:

    jsondata: {'dataurl': 'data:image/png,fffff'}

optional args:

    path:   custom/path/to/foo{0:06d}.png
    grayscale: 1
    makedirs: 1

returns: application/json
{
    success: true,
    path: "image000x.png"    
}
"""

from __future__ import print_function
import cgitb; cgitb.enable()
import os, cgi, urllib, json, re, sys
from base64 import decodestring

data_url_pat = re.compile(r"^data:(?P<type>\w+/\w+)?(?:;(?P<base64>base64))?,")

def decode_data_url (url):
    m = data_url_pat.search(url)
    if m:
        d = m.groupdict()
        data = url[m.end():]
        if d.get("base64"):
            d['data'] = decodestring(data)
        else:
            d['data'] = urllib.unquote(data)
        return d

def grayscale (data):
    from PIL import Image
    from StringIO import StringIO
    im = Image.open(StringIO(data))
    im = im.convert("L")
    fout = StringIO()
    im.save(fout, format="png")
    return fout.getvalue()

method = os.environ.get("REQUEST_METHOD", "GET")
fs = cgi.FieldStorage()
path = fs.getvalue("path", ".")
filename = fs.getvalue("filename", "image{0:04d}.png")
grayscale = fs.getvalue("grayscale")
makedirs = fs.getvalue("makedirs")
result = {'success': False};

if method == "POST":
    data = json.loads(fs.getvalue("jsondata"))
    # When path is a pattern (includes {0}), ensure unique
    if "{0" in filename:
        fpath, filename = os.path.split(filename)
        # let a path in filename override the (default) path setting
        if fpath:
            path = fpath
        x = 1
        while True:
            imgpath = os.path.join(path, filename.format(x))
            if not os.path.exists(imgpath):
                break
            x += 1
    else:
        imgpath = os.path.join(path, filename)

    if makedirs:
        d, _ = os.path.split(imgpath)
        try:
            os.makedirs(d)
        except OSError:
            pass

    # print ("Saving image to {0}".format(imgpath), file=sys.stderr)
    imgdata = decode_data_url(data['dataurl'])
    if grayscale:
        imgdata = grayscale(imgdata)
    with open(imgpath, "wb") as f:
        f.write(imgdata['data'])
        result['path'] = imgpath
        result['success'] = True
    print 

print ('Content-Type: application/json')
print ()
print (json.dumps(result))

