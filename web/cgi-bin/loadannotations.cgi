#!/usr/bin/env python

import json, re, os
import cgi


fs = cgi.FieldStorage()
path = fs.getvalue('path', '.')
objpath = os.path.join(path, fs.getvalue("objpath", "obj-info.txt"))
bgpath = os.path.join(path, fs.getvalue("bgpath", "bg.txt"))
infopath = os.path.join(path, fs.getvalue("infopath", "info/info.lst"))

data = {}
data['frames'] = frames = []

linepat = re.compile(r"^(?P<src>.+?)\s*(?:(?P<objcount>\d+)\s(?P<rects>.+))?$")
def process_rects (r):
    ret = []
    rectpat = re.compile(r"(?P<x>\d+) (?P<y>\d+) (?P<width>\d+) (?P<height>\d+)")
    for m in rectpat.finditer(r):
        d = m.groupdict()
        ret.append({
            'x': int(d['x']),
            'y': int(d['y']),
            'width': int(d['width']),
            'height': int(d['height'])
        })
    return ret

paths = [bgpath, objpath, infopath]
for p in paths:
    dirpath, _ = os.path.split(p)
    try:
        with open(p) as f:
            for line in f:
                line = line.strip()
                if line and not line.startswith("#"):
                    m = linepat.search(line)
                    if m:
                        d = m.groupdict()
                        src = os.path.join(dirpath, d['src'])
                        # src = os.path.relpath(src, path)
                        frame = {'src': src}
                        if d.get("rects"):
                            frame['rects'] = process_rects(d.get("rects"))
                        else:
                            frame['rects'] = []
                            
                        frames.append(frame)
    except IOError:
        pass

# sort frames by src
frames.sort(key=lambda x: x['src'])

print "Content-type:application/json"
print
print json.dumps(data, indent=2)
