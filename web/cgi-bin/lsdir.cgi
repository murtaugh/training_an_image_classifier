#!/usr/bin/env python

import cgi, json, os

dd = os.listdir(".")
dd.sort()
ex = set(("cgi-bin", "js"))
dd = [x for x in dd if os.path.isdir(x) and x not in ex]

print "Content-type:application/json"
print
print json.dumps(dd)